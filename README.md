# Windows 10 with office 365 and teams

___If___ you have a Linux laptop or Chromebook as your daily driver,
___and___ you need to use Microsoft Office, for example to collaborate with your Microsoft minded Enterprise,
___then___ this is the place to create you own docker container / customized Virtual Machine.

## How
Here we collect scripts (docker, powershell) for creating a Windows 10 containers or virtualbox VM, 
with the FAT-client of MS-Office 365 and MS-Teams, 
so that you can run the apps isolated and able to use office on every host you want to.

## Why? What's the Use Case?

1. Microsoft Office
    1. TL;DR for MS-Office functionality you can only use Macos or Windows FAT-clients.
    2. The web-client of MS-office (Powerpoint, Excel, Word) only works for small edits. The layout of a document is for example not supported.
    3. The Android-client of MS-office is a wrapper around the web-client.
    4. Office-365 for Business does not support integration with LibreOffice via for example a sharepoint/onedrive URI,
        therefor opening files from Office-365 including online collaboration is not possible.
    5. If you want to run Linux on your host/laptop, 
        and want to run the Macos or Windows FAT-client, 
        then your only option is a Windows Virtual Machine (VM), 
        since a Macos VM is not compliant in regards to Apple License Model. 
    6. If you want to use a Chromebook (Chrome OS) as your host/laptop,
        then you're limited in the usage of Office-365,
        since Chromebook does not support VM’s,
        and MS-office documents are only supported via web, Android or LibreOffice,  
2. Microsoft Teams
    1. TL;DR Microsoft only support the Windows version of the Teams client with (new) features.
    1. The web-client of teams does not support backgrounds.
    2. The Linux-client of teams does not support backgrounds.
    3. The Android-client of teams on a Chromebook does not support cam and audio.
    4. The web-client only supports one picture in the gallery view.
    5. The Linux-client only supports 2x2 pictures in the gallery view.
    6. The android-client only supports 1 picture in the gallery view.
    7. If you want more than 4 pictures in your gallery view, and monthly upgrades, you can only use the Windows FAT-client.

## Guidelines - Security
1. Secure the host machine
    1. Encrypt the harddisk (pw at startup).
    1. Password on user session (pw at startup and lockscreen).
    1. Harden your host machine according to company policy.
        1. Overall good practice is to use a VPN provider and use a Password Manager.
1. Secure the guest machine
    1. Encrypt the harddisk (pw at startup).
    1. Password on user session (pw at startup and lockscreen).
    1. Harden your guest machine according to company policy.

## Guidelines - Creating the guest machine
1. Download Windows
    1. Download via Docker container
    1. Download in a Docker build script
    1. Download a Virtual Machine provided by Microsoft
    1. Download an ISO files provided by Microsoft
1. Install Windows
1. Activate Windows
    1. or do not activate it, and it will stop working af 90 days.
1. Download and Install Microsoft Offce 365.
    1. Manual via the GUI.
    1. Automated via the Office Deployment Tool (ODT).
        1. run the script via a Docker build script.
        1. run the script via powershell in the VM.
1. Activate Microsoft Office 365.
1. Download and Install Microsoft Teams.
1. Create snapshot of installation and backup the snapshot.
1. Configure Windows.
    1. enable and test Virtual Box shared clipboard (host to guest)
    1. Set the timezone
    1. Set user password
    1. Set startup guest machine into lock screen
    1. Set harddisk encryption (Bitlocker), via a powershell script
